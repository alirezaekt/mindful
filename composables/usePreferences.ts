const usePreferences = () => {

    const config = useRuntimeConfig()
    
    const preferences = useState("preferences",() => { return {
      //default preferences
        "Widgets": {
          "Weather":true,
          "Pomodoro":true,
          "To do":true,
          "Sounds":true,
          "Notes":true,
        },
        "Wallpaper":"Unsplash",
        "PomoTimer":{
          focus: 25,
          shortBreak: 5,
          longBreak: 15
        }
      }
    })
    
    const setPreference = (category, item, value) => {
      if (category === 'Widgets') {
        preferences.value[category][item] = value
      } else {
        preferences.value[item] = value
      }
      localStorage.setItem('mindfulprefs', JSON.stringify(preferences.value));
    }

    const loadPreferences = () => {
      if (localStorage.getItem('mindfulprefs')) {
        preferences.value = JSON.parse(localStorage.getItem('mindfulprefs'))
      }
    }

    // getting a random photo from unsplash for background
    const getWallpaper = async () => {

        let backgroundImage = "url("+"https://images.unsplash.com/photo-1650742031603-ced6b0f3a51a?crop=entropy&cs=srgb&fm=jpg&ixid=MnwzMjU1MDd8MHwxfHJhbmRvbXx8fHx8fHx8fDE2NTE2NTAwNTc&ixlib=rb-1.2.1&q=85"+")"
        let backgroundAuthor = {
            name:"Mathias P.R. Reding",
            link:"https://unsplash.com/photos/aCqYDB_3Dq8"
        }
        try {
          const { pending, data:unsplash, error, refresh } = await useFetch(
            "https://api.unsplash.com/photos/random",
            {
              params: {
                client_id:config.unsplashID,
                client_secret:config.unsplashSecret
              }
            }
          )
          backgroundImage = "url("+unsplash.value['urls'].full+")"
          backgroundAuthor = {
            name:unsplash.value['user'].name,
            link:unsplash.value['links'].html
          }
        } catch (error) {
          console.log(error)
        }
        return { backgroundImage, backgroundAuthor }
    }
    return{
        getWallpaper,
        preferences,
        setPreference,
        loadPreferences
    }
}
export default usePreferences
