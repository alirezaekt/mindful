const useAuth = () => {
    const { auth } = useSupabaseClient()
    
    const signUp = async ({ email, password, ...metadata }) => {
        const { user:u, error } = await auth.signUp({ email, password },{ data: metadata })
        if (error) throw error
        return u
    }

    const signIn = async ({email, password}) => {
        const { user:u, error } = await auth.signIn({ email, password })
        if(error) throw error
        return u
    }

    const forgotPassword = async (email) => {
        const { data, error } = await auth.api.resetPasswordForEmail(email)
        return {data, error}
    }
    const updatePassword = async (access_token, new_password) => {
        const { data, error } = await auth.api.updateUser(access_token, { password : new_password })
        return {data, error}
    }

    const oSignIn =async (provider:'github'|'google'|'gitlab'|'bitbucket') => {
        const { user:u, error } = await auth.signIn({ provider: provider })
        if(error) throw error
        return u
    }

    const signOut = async () => {
        const { error } = await auth.signOut()
        if ( error ) throw error
    }

    const ResetpassHashToParam = (routeHash) => {
        return {
            name:'resetpassword',
            query:{
                access_token : routeHash.split('&')[0].split('=')[1],
                expires_in : routeHash.split('&')[1].split('=')[1],
                refresh_token : routeHash.split('&')[2].split('=')[1],
                token_type : routeHash.split('&')[3].split('=')[1],
                t_type : routeHash.split('&')[4].split('=')[1]
            }
        }
    }

    return {
        signUp,
        signIn,
        signOut,
        forgotPassword,
        updatePassword,
        oSignIn,
        ResetpassHashToParam
    }
}

export default useAuth
