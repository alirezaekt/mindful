# Mindful - a sleek, customizable productivity dashboard

Built using Nuxt 3 + Supabase

Current widgets: Today (Weather and quote), To Do List, Pomodoro, Notes, Ambient Sound



Try the [Demo](https://mindful.coolconcepts.tech)
## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```
set the API keys in .env file:
```
SUPABASE_URL=***
SUPABASE_KEY=***
WEATHERMAP_KEY=***
UNSPLASH_CLIENTID=***
UNSPLASH_CLIENTSECRET=***

```


## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

