const useTodos = () => {

    // const { data:todos } = useFetch("/api/todo")
    const { data:todos, refresh } = useAsyncData("todo", async () => {
        return $fetch("/api/todo")
    })
    const addTodo = async (item) => {
        if (!item) return
        await $fetch("/api/todo",{
            method: "POST",
            body: { item: item }
        })
        // refresh()
    }
    const updateTodo = async (id) => {
        await $fetch(`/api/todo/${id}`,{
            method: "PUT",
        })
        // refresh()
    }
    const deleteTodo = async (id) => {
        await $fetch(`/api/todo/${id}`,{
            method: "DELETE",
        })
        // refresh()
    }    
    const refreshTodo = async () => {
        refresh()
    }
    return {
        todos,
        addTodo,
        updateTodo,
        deleteTodo,
        refreshTodo
    }
}

export default useTodos
